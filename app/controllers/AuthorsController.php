<?php

class AuthorsController extends BaseController {

	public function getList()
	{
		//
		$authors = Author::all();
		return View::make('authors.list',array("authors"=>$authors));
		
	}
	
	
	public function create(){
		
		$author = new Author();
		
		return View::make('authors.create',array('author'=>$author));
		}
		
		
	public function save(){
		
		
		
		$a = new Author();
		$a->name = "Teste auto";
		$a->bio = "Teste gerado automaticamente";
		
		$a->save();
		
		
		return Redirect::to('/authors');
		
		}

}
