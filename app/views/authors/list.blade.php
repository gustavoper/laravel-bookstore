<html lang="en">
	<head>
		<title>Authors List</title>
	</head>
	<body>
		<h1>Authors list!</h1>
		{{ link_to_route('authors.create','New Author') }}
		<ul>
		@foreach ($authors as $author)
			<li>{{ $author->name }} </li>
			
		@endforeach
		
		</ul>
	</body>
</html>
