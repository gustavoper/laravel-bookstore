<html lang="en">
	<head>
		<title>Authors List</title>
	</head>
	<body>
		<h1>New Author!</h1>

		{{ Form::open(array('route'=>'authors.save')) }}
		
		{{ Form::label('name','Name')  }}
		{{ Form::text('name')  }}
		
		{{ Form::label('bio','Biography')  }}
		{{ Form::text('bio')  }}
		
		{{ Form::submit('Submit!') }}
		
		
	</body>
</html>
